In this article, we will use the APIs for interacting with the .NET compiler
[Roslyn](https://github.com/dotnet/roslyn), to read a C# project file, parse
all source files and write a list of classes to the console.

# What is Roslyn?

Roslyn is the name for the compiler platform for .NET. It consists of the
compiler itself and a powerful set of APIs to interact with the same. The Roslyn
platform is open source and hosted at [github.com/dotnet/roslyn](https://github.com/dotnet/roslyn).
The compiler is part of every .NET installation. The APIs to interact with the
compiler are available via NuGet (see the [Roslyn repository](https://github.com/dotnet/roslyn)
for details).

If you are interested in the details of the .NET compiler platform, you can
check out the [Roslyn Overview](https://github.com/dotnet/roslyn/wiki/Roslyn-Overview).
The information provided there is very detailed.

# Create a .NET Core project

Let's get started right and away and as a first step, create a .NET Core project
for toying around.

I am using [Visual Studio Code](https://code.visualstudio.com/) as a IDE with
the [Microsoft C# extension](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp)
installed. For the examples, I am also making use of the command line and `dotnet`
CLI. If you are familiar with another IDE, e.g. Visual Studio, Rider or even Vim,
it will be fine.

We create a new .NET Core project by opening a command line, navigate to some
directory where you usually have your source code and enter:

```sh
mkdir find-classes
cd find-classes
dotnet new console
```

This will scaffold a new console project in the folder `find-classes`. After the
commands have finished, you should see something like this:

![](./01_create-core-project.png)

If you are working with [git](https://git-scm.com/), you'll also want to add a
`.gitignore` file in the root folder of your project. So fire up your IDE, open
the project and add a new file `.gitignore` to your project root:

```
*.suo
*.vshost.exe
*~
*.user
obj
*.swp
bin
_Resharper.*
packages
.fake
build
dist
out
.vs
```

To check if the project is working, you can enter `dotnet run` in the projects
root folder or run it from your IDE. You should see `Hello World!` printed to
the console.

# Iteration I: Read a source file and get the classes

When I am coding, I usually work in small incremental steps that build on top of
each other. So, let us start small and build on top of it. Our first goal will
be to just parse a source file and print out the classes to the console.

First, we need to add some packages to be able to do Roslyn code analysis. Again,
switch to your terminal, navigate to the projects root folder and type:

```sh
dotnet add package Microsoft.CodeAnalysis
dotnet add package Microsoft.Net.Compilers
```

The `dotnet` CLI should download the packages from the NuGet repository and add
them as a reference to your project.

![](./02_add-roslyn-packages.png)

If you have never used the libraries, your output might look different, but the
two commands should succeed and the packages be added to `find-classes.csproj`.

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>netcoreapp3.1</TargetFramework>
    <RootNamespace>find_classes</RootNamespace>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="Microsoft.CodeAnalysis" Version="3.5.0" />
    <PackageReference Include="Microsoft.Net.Compilers" Version="3.5.0">
      <IncludeAssets>runtime; build; native; contentfiles; analyzers; buildtransitive</IncludeAssets>
      <PrivateAssets>all</PrivateAssets>
    </PackageReference>
  </ItemGroup>

</Project>
```

Go back to your code and change `Program.cs` like so:

```csharp
using System.IO;
using System.Linq;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace FindClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            var sourceFilePath = args[0];
            var source = File.ReadAllText(sourceFilePath);

            var syntaxTree = CSharpSyntaxTree.ParseText(source).WithFilePath(sourceFilePath);
            var syntaxTreeRoot = syntaxTree.GetRoot();

            foreach (var classDeclaration in syntaxTreeRoot.DescendantNodes().OfType<ClassDeclarationSyntax>())
            {
                System.Console.WriteLine(classDeclaration.Identifier.ValueText);
            }
        }
    }
}
```

Please note that I have changed the namespace from `find-classes` to `FindClasses`.
We use the first command line argument as the path to our source file we would
like to analyze. Afterwards we use `File.ReadAllText(sourceFilePath)` to read
the whole source code.

The next two lines use the `CSharpSyntaxTree` from Roslyn to create a syntax
tree and get access to the root node.

```csharp
var syntaxTree = CSharpSyntaxTree.ParseText(source).WithFilePath(sourceFilePath);
var syntaxTreeRoot = syntaxTree.GetRoot();
```

A syntax tree is a representation of your source code and literally a tree data
structure, where non-terminal structural elements parent other elements. It is
the primary structure used for compilation and code analysis. Each syntax tree
is made up of nodes, tokens, and trivia. For more information see [Syntax Trees](https://github.com/dotnet/roslyn/wiki/Roslyn-Overview#syntax-trees).

One type of nodes that help us find all class declarations are the
`ClassDeclarationSyntax` nodes. In the `foreach`-loop we iterate through all of
them and print the identifier to the console.

Now, let's try it out. In your terminal, navigate to the root folder of your
project and type:

```sh
dotnet run Program.cs
```

You should see `Program` written to the console as it currently is the only
class in `Program.cs`.

![](./03_dotnet-run-hello-roslyn.png)

# Iteration II: Use the visitor pattern

Getting all the descendant nodes with `syntaxTreeRoot.DescendantNodes()` is one
way to query for all classes in a source file. There is another, more
object-oriented way to do things, we can use the [Visitor Design Pattern](https://en.wikipedia.org/wiki/Visitor_pattern)
to achieve the same result.

Let's create a new source file `ClassCollector.cs` in our project and add the
following code.

```csharp
using System.Collections.Generic;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace FindClasses
{
    internal class ClassCollector : CSharpSyntaxWalker
    {
        public List<string> ClassNames { get; }

        public ClassCollector()
        {
            ClassNames = new List<string>();
        }

        public override void VisitClassDeclaration(ClassDeclarationSyntax node)
        {
            ClassNames.Add(node.Identifier.ValueText);

            base.VisitClassDeclaration(node);
        }
    }
}
```

The class is derived from `CSharpSyntaxWalker` and overrides exactly one method
`VisitClassDeclaration(ClassDeclarationSyntax node)`. This method is called,
whenever a class declaration syntax node is found in the syntax tree. To make
things easier, we just add the class name to the list of class names.

Ensure that you call the base method in the override, otherwise you will get
strange results as the depth-first recursive search of `CSharpSyntaxWalker` is
not going to work properly.

Now let's use the `ClassCollector` and change `Program.cs`.

```csharp
using System.IO;
using Microsoft.CodeAnalysis.CSharp;

namespace FindClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            var sourceFilePath = args[0];
            var source = File.ReadAllText(sourceFilePath);

            var syntaxTree = CSharpSyntaxTree.ParseText(source).WithFilePath(sourceFilePath);
            var syntaxTreeRoot = syntaxTree.GetRoot();

            var classCollector = new ClassCollector();
            classCollector.Visit(syntaxTreeRoot);

            foreach (var classCame in classCollector.ClassNames)
            {
                System.Console.WriteLine(className);
            }
        }
    }
}
```

This design is a little bit nicer as we now have the code to extract the
class names nicely separated into `ClassCollector`. In `Main` we just let the
Visitor walk the nodes in the syntax tree and collect all the class names.
Afterwards, we print them to the console.

Now run the program. You'll still see only `Program` as the output. The reason
for that is, that we still only parse `Program.cs`. We will change this in the
next chapter.

# Iteration III: Open a project and parse all documents

A C# project usually does not include only one file. It is one project consisting
multiple source files, sometimes even a solution with multiple projects and source
files.

You will need some additional references to load project files and solutions. Open
up a console, navigate to the project root folder and type:

```sh
dotnet add package Microsoft.Build.Locator
dotnet add package Microsoft.CodeAnalysis.CSharp.Workspaces
dotnet add package Microsoft.CodeAnalysis.Workspaces.MSBuild
```

This will add three more references: `Microsoft.Build.Locator`, 
`Microsoft.CodeAnalysis.CSharp.Workspaces` and `Microsoft.CodeAnalysis.Workspaces.MSBuild`.
Your `find-classes.csproj` should now look like this.

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>netcoreapp3.1</TargetFramework>
    <RootNamespace>find_classes</RootNamespace>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="Microsoft.Build.Locator" Version="1.2.6" />
    <PackageReference Include="Microsoft.CodeAnalysis" Version="3.5.0" />
    <PackageReference Include="Microsoft.CodeAnalysis.CSharp.Workspaces" Version="3.5.0" />
    <PackageReference Include="Microsoft.CodeAnalysis.Workspaces.MSBuild" Version="3.5.0" />
    <PackageReference Include="Microsoft.Net.Compilers" Version="3.5.0">
      <IncludeAssets>runtime; build; native; contentfiles; analyzers; buildtransitive</IncludeAssets>
      <PrivateAssets>all</PrivateAssets>
    </PackageReference>
  </ItemGroup>

</Project>
```

Let's now go ahead and change the code to be able to read a project file instead
of a single source file. Open `Program.cs` in your favorite editor and change it
to:

```csharp
using System.Linq;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis.MSBuild;

namespace FindClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            var projectFilePath = args[0];

            MSBuildLocator.RegisterDefaults();
            var workspace = MSBuildWorkspace.Create();
            var project = workspace.OpenProjectAsync(projectFilePath).Result;

            var classCollector = new ClassCollector();
            foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
            {
                var syntaxTree = document.GetSyntaxTreeAsync().Result;
                var syntaxTreeRoot = syntaxTree.GetRoot();

                classCollector.Visit(syntaxTreeRoot);
            }

            foreach (var className in classCollector.ClassNames)
            {
                System.Console.WriteLine(className);
            }
        }
    }
}
```

Some things have changed now. For starters, our argument is no longer a single
source file, but the whole `.csproj` project file. The lines

```csharp
MSBuildLocator.RegisterDefaults();
var workspace = MSBuildWorkspace.Create();
var project = workspace.OpenProjectAsync(projectFilePath).Result;
```

are opening the project. As we now have multiple source files, we need to
iterate all the documents in the project and filter those that support a syntax
tree. We do this with

```csharp
foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
{
    var syntaxTree = document.GetSyntaxTreeAsync().Result;
    var syntaxTreeRoot = syntaxTree.GetRoot();

    classCollector.Visit(syntaxTreeRoot);
}
```

As the `classCollector` is not recreated, it accumulates all the class names.
Now run the program. You should see two types, each one on it's own row:
`Program` and `ClassCollector`as the output.

![](./04_dotnet-run-project-file.png)

In the final chapter we are going to speed up source file parsing by making use
of multi-threading.

# Bonus: Speeding up things with async/await

In the previous chapter we did not really make use of the multi-threading
capabilities. Instead we immediately resolved the
[futures](https://en.wikipedia.org/wiki/Futures_and_promises) and therefore did
everything synchronously. As a bonus let's now refactor our code to analyze
the syntax tree of each source file in parallel.

As usual we are doing it in small incremental steps. Let's first move everything
that we want to do asynchronously in its own function. Open `Program.cs` in your
editor and change it like this:

```csharp
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis.MSBuild;

namespace FindClasses
{
    class Program
    {
        private static async Task<List<string>> RunAsync(MSBuildWorkspace workspace, string projectFilePath)
        {
            var project = await workspace.OpenProjectAsync(projectFilePath);

            var classCollector = new ClassCollector();
            foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
            {
                var syntaxTree = document.GetSyntaxTreeAsync().Result;
                var syntaxTreeRoot = syntaxTree.GetRoot();

                classCollector.Visit(syntaxTreeRoot);
            }

            return classCollector.ClassNames;
        }

        static void Main(string[] args)
        {
            var projectFilePath = args[0];

            MSBuildLocator.RegisterDefaults();
            var workspace = MSBuildWorkspace.Create();
            var classNames = RunAsync(workspace, projectFilePath).Result;

            foreach (var className in classNames)
            {
                System.Console.WriteLine(className);
            }
        }
    }
}
```

From a multi-threading perspective we have not improved anything up to now. We
just did a small refactoring to separate our code we would like to execute 
asynchronously into `RunAsync`.

The syntax trees are still analyzed one after the other. Let's further refactor
the method `RunAsync` and extract a new method:

```csharp
// <-- snip -->
using Microsoft.CodeAnalysis;
// <-- snap -->

        // <-- snip -->
        private static async Task<List<string>> RunAsync(MSBuildWorkspace workspace, string projectFilePath)
        {
            var classNames = new List<string>();

            var project = await workspace.OpenProjectAsync(projectFilePath);
            foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
            {
                classNames.AddRange(GetClassNamesAsync(document).Result);
            }

            return classNames;
        }
        
        private static async Task<List<string>> GetClassNamesAsync(Document document)
        {
            var syntaxTree = await document.GetSyntaxTreeAsync();
            var syntaxTreeRoot = syntaxTree.GetRoot();

            var classCollector = new ClassCollector();
            classCollector.Visit(syntaxTreeRoot);

            return classCollector.ClassNames;
        }
        // <-- snap -->
```

Again, slightly better as we now have a asynchronous function that is executed
for each source file. But, we are still not there yet. Let us now execute the
analysis in parallel first and collect the results later on by further
refactoring `RunAsync`:

```csharp
        // <-- snip -->
        private static async Task<List<string>> RunAsync(MSBuildWorkspace workspace, string projectFilePath)
        {
            var classNames = new List<string>();
            var analysisTasks = new List<Task<List<string>>>();

            var project = await workspace.OpenProjectAsync(projectFilePath);
            foreach (var document in project.Documents.Where(d => d.SupportsSyntaxTree))
            {
                analysisTasks.Add(GetClassNamesAsync(document));
            }

            Task.WaitAll(analysisTasks.ToArray());

            foreach (var analysisTask in analysisTasks)
            {
                if (!analysisTask.IsCompletedSuccessfully)
                {
                    continue;
                }

                classNames.AddRange(analysisTask.Result);
            }

            return classNames;
        }
        // <-- snap -->
```

The method is now first starting all analysis in parallel and afterwards using
`Task.WaitAll(analysisTasks.ToArray())` to wait for each task to finish. Then
the results are collected and accumulated in `classNames` which is returned from
`RunAsync`.

There we are! For each source file, the syntax tree is now analyzed in parallel.

# Summary and Outlook

In this post we learned the basics of syntax tree analysis with Roslyn. We first
parsed a single C# source file and got a list of class names. Afterwards we used
the Visitor Design Pattern with a implementation of `CSharpSyntaxWalker`.

Then we moved on to loading a complete project and analyzing multiple source
files. In the end we also used async/await to speed up our code with
multi-threading.
