# Outline

Make it a series. It is going to be huge. Nobody reads such a huge article.

1. Meaning of filters
2. Your best filter
   * say 'no'
   * it is possible the hardest thing and I am also struggling a lot
   * you are managing your energy
   * either: you do not do things right as you rush them or things slip through
3. Filter your channels
   * Get rid of notifications from e-mail and chat during deep working hours
   * Only read e-mails 1-2 times a day
4. Filter your e-mails if you have a big inbox and feel overwhelmed:
   * Filter by day: today, yesterday, last week, specific date
   * Different ways: use folders, use the filter function of Outlook
5. Consolidate your pile and variations of todo lists (personally it helps me to
   write it by hand, others might prefer it digitally)
6. Block your calendar for a big chunk of time to organize your work. Honestly,
   you can do it, right. You can also go on offsite-workshops and not be 
   reachable for a whole day. Status meetings can wait. (Sharpen the axe)
7. Divide-and-Conquer or Carpacio-Method or however you call it
8. Try to work with small victories
9.  Use the Seinfeld method
10. Really listen to your peers:
    * listen carefully
    * listen first, talk later
    * be patient
    * do not get distracted
    * avoid distractions
11. Summary
   * I have said in the beginning that there was a need for me to be more
     structured and organized, but maybe the remote work just made an issue
     transparent that has been there for years.

# Notes

* Make first a article about "listening"?
* Make this article a series?
* 
* Title: Focused work: Use filters when you feel overwhelmed by your topics
* Slug: focused-work-use-filters-when-you-feel-overwhelmed

I also try to apply this to conversations (meetings) either in person or
virtually. If I would start doing other things in parallel, e.g. looking at my
smartphone or reading e-mails, I am not fully focusing on my peers, thus wasting
my energy. By the way, you would also waste the energy of your peers in this
case as they might have to explain things more than once because you are not
fully following the conversation.
