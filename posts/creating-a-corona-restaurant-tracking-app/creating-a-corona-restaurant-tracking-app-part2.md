# Guest registration form

In [part 1](https://walters-blog.web.app/article/creating-a-corona-restaurant-tracking-app-part1)
I have described the idea and requirements for the Corona restaurant tracking
app. Then I have setup the project in [React](https://reactjs.org/) and made 
some preparations for the UI with [React Material-UI](https://material-ui.com/).

As I have already described in the previous parts, the project is entirely open
source. You can check it out on [gitlab](https://gitlab.com/walter76/guest-tracker).

In this part, I will build out the form for registering a guest. I will not yet
introduce some backend. This I save for the next part. So, let's get started!

## Preparation steps

The first feature will be to implement the guest registration form. But before
doing that, I need to do a little bit of refactoring as I have a certain code
structure I am following. See [Structure files as feature folders or ducks](https://redux.js.org/style-guide/style-guide#structure-files-as-feature-folders-or-ducks)
in the [Redux sytle guide](https://redux.js.org/style-guide/style-guide) for
more details on that. 

I move `App.js` from `src` to `src/app`. Then I need to update the import in 
`index.js` to point to `src/app/App.js`.

```js
import App from './app/App'
```

Back to the guest registration form. My code is structured in a feature-wise
manner. So I create a folder `src/features/registration`. Inside this folder, I
create a new file `Registration.js` which will be the main component for the
guest registration form.

```jsx
import React from 'react'

export default () => <div>Guest registration</div>
```

For now, I will just use this component in `App.js`.

```jsx
import React from 'react'

import Registration from '../features/registration/Registration'

export default () => (
  <div>
    <div>guest-tracker-app</div>
    <Registration />
  </div>
)
```

Starting the app with `yarn start` will show the following screen which is as
good as it gets for just simply laying the foundation for the guest registration
form.

![](./09_registration-form-init.png)

## Styling and a first layout

I open `Registration.js` and put this structure for styling in place for the
registration component.

```jsx
import React from 'react'

import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  root: {
    margin: 'auto',
    width: '480px'
  }
})

export default () => {
  const classes = useStyles()

  return <div className={classes.root}>Guest registration</div>
}
```

I add a style `root` which will set the width of the component to 480 pixels and
center it with setting the margin to `auto`. Then those styles are applied to
the root `<div>`-container.

Next I start working on the input form itself and change `Registration.js` as
follows.

```jsx
import React, { useState } from 'react'

import { makeStyles } from '@material-ui/core/styles'
import { Paper, Typography, TextField } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 'auto',
    width: '480px'
  },
  guestRegistration: {
    padding: theme.spacing(2)
  },
  registrationForm: {
    display: 'flex',
    flexDirection: 'column',

    '& > *': {
      marginTop: theme.spacing(2)
    }
  }
}))

export default () => {
  const [lastname, setLastname] = useState('')
  const [firstname, setFirstname] = useState('')

  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Paper>
        <div className={classes.guestRegistration}>
          <Typography variant="h5" noWrap>
            Guest registration
          </Typography>
          <form>
            <div className={classes.registrationForm}>
              <TextField
                fullWidth
                required
                autoFocus
                name="lastname"
                placeholder="Lastname"
                label="Lastname"
                size="small"
                value={lastname}
              />
              <TextField
                fullWidth
                required
                name="firstname"
                placeholder="Firstname"
                label="Firstname"
                size="small"
                value={firstname}
              />
            </div>
          </form>
        </div>
      </Paper>
    </div>
  )
}
```

I have wrapped the whole content in a `<Paper>`-component to give it a nice
rounded beveled border. Inside I added another `<div>` to get control over the
format of the content. For now I just add a padding there (style
`guestRegistration`). Next there is a header with a `<Typography>`-component and
below it starts the `<form>`.

Inside the `<form>` I have added another `<div>` which will be my flex-container
for the form elements. The style for it is quite interesting. Besides defining a
flex-container with a direction of the items in columns, it has this cryptic
part:

```js
'& > *': {
  marginTop: theme.spacing(2)
}
```

This tells the styling engine that all children below the element should get a
top-margin. That way, I give all input fields a top-margin of `theme.spacing(2)`.
Inside the final `<div>` I create two `<TextField>`-components. One for the
firstname and another for the lastname.

The last interesting part is that I have introduced the `useState`-hook to make
this component stateful.

```js
const [lastname, setLastname] = useState('')
const [firstname, setFirstname] = useState('')
```

Those variables will capture the state of the form elements while displaying the
guest registration component.

## Managing state

Starting it up, there is one issue with our code. In developer mode I see the
following warning in the integrated terminal.

![](./10_unused-set.png)

Although I have connected the `value` attribute of the `<TextField>` components
I did not yet take care to actually store the value in the state.

For that, I add the following utility method somewhere at the top of the file
below the imports.

```js
// <-- snip -->
const onChange = (f) => (e) => f(e.target.value)
// <-- snap -->
```

Later on, we will extract the method to its own module. But for now it is just
fine to have it here.

Then I change the two `<TextField>` components as follows.

```jsx
<TextField
  fullWidth
  required
  autoFocus
  name="lastname"
  placeholder="Lastname"
  label="Lastname"
  size="small"
  value={lastname}
  onChange={onChange(setLastname)}
/>
<TextField
  fullWidth
  required
  name="firstname"
  placeholder="Firstname"
  label="Firstname"
  size="small"
  value={firstname}
  onChange={onChange(setFirstname)}
/>
```

This makes the warning go away as I now also store the manipulated state. 

The function `onChange` is a [higher-order function](https://en.wikipedia.org/wiki/Higher-order_function#JavaScript)
which takes the function passed to it and returns a new function calling the
passed in function with the argument `e.target.value`. This is a very convenient
way to not have always write something like this.

```jsx
<TextField
  onChange={(e) => setFirstname(e.target.value)} />
```

## Backend service

I will now introduce the logic to call a backend service later on. For that, I
create a new folder `client` under the root source folder `src`. The file I put
there is called `clientService.js` in lack of a better idea for a name.

```js
export default {
  registerGuest: async (firstname, lastname) => {
    console.log(`Registering guest ${firstname}, ${lastname}`)
  }
}
```

This is not doing much. It just provides a method `registerGuest` which can be
used later on to actually call a backend service function. Now I need the
possibility to call it from the React component. To not make this whole thing
too complex, I will not use [Redux](https://redux.js.org/). Instead I will
manage the state in the components. But, I will use a hook `useClientService` to
get access to the `clientService`. For that, I create a new folder `common`
under the root source folder `src`. Next, I put a file `useClientService.js`
there.

```js
import clientService from '../client/clientService'

export default () => clientService
```

Now everything is in place that we actually can submit the form and call the
backend service.

## Submitting the form

I will open `Registration.js` in the editor and make a few changes. First, I
introduce a method `onSubmit`.

```js
// <-- snip -->
import useClientService from '../../common/useClientService'
// <-- snap -->

// <-- snip -->
export default () => {
  const clientService = useClientService()

  const [lastname, setLastname] = useState('')
  const [firstname, setFirstname] = useState('')

  const onSubmit = (event) => {
    clientService.registerGuest(firstname, lastname)

    event.preventDefault()
  }
// <-- snap -->
```

This method will call the `registerGuest` method from the `clientService`. Next,
I have to change the form and introduce a button to submit it.

```jsx
// <-- snip -->
import { Paper, Typography, TextField, Button } from '@material-ui/core'
// <-- snap -->

// <-- snip -->
return (
  <div className={classes.root}>
    <Paper>
      <div className={classes.guestRegistration}>
        <Typography variant="h5" noWrap>
          Guest registration
        </Typography>
        <form onSubmit={onSubmit}>
          <div className={classes.registrationForm}>
            <TextField
              fullWidth
              required
              autoFocus
              name="lastname"
              placeholder="Lastname"
              label="Lastname"
              size="small"
              value={lastname}
              onChange={onChange(setLastname)}
            />
            <TextField
              fullWidth
              required
              name="firstname"
              placeholder="Firstname"
              label="Firstname"
              size="small"
              value={firstname}
              onChange={onChange(setFirstname)}
            />
            <Button type="submit" color="inherit">
              Register
            </Button>
          </div>
        </form>
      </div>
    </Paper>
  </div>
)
// <-- snap -->
```

Running the app and hitting the register button now looks like that. As you can
 see, the logic is working and on the console the output
 `Registering guest Walter, Stocker` is written.

![](./11_guest_register_onsubmit.png)

So, now I have everything in place and can code the rest of the form.

# Summary

Done with part 2. I now have a working guest registration form. It is not yet
connected to a backend service and will actually not save any data. We will fix
this in the next part of the article.
