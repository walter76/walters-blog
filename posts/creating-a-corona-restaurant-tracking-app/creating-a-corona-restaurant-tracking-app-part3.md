---
title: Creating a Corona restaurant guest registration app - Part 3
---

# Connecting to a backend service

Today I am publishing part 3 of the series where I will build a Corona restaurant tracking app. If you have discovered those posts just now, and am curious on the other parts, you can find them here:

* [Part 1- Creating a Corona restaurant guest registration app](https://walters-blog.web.app/article/creating-a-corona-restaurant-tracking-app-part1)
* [Part 2 - Guest registration form](https://walters-blog.web.app/article/creating-a-corona-restaurant-tracking-app-part2)

As I have already described in the previous parts, the project is entirely open
source. You can check it out on [gitlab](https://gitlab.com/walter76/guest-tracker).

In this part, I will connect my client to a real backend service and with that enable the storage of some real data.

## The guest registration form

In the previous post I have started creating the guest registration form. I have finished the first version of the form.

```jsx
import React, { useState } from 'react'

import { makeStyles } from '@material-ui/core/styles'
import {
  Paper,
  Typography,
  TextField,
  Button,
  InputAdornment
} from '@material-ui/core'
import PhoneIcon from '@material-ui/icons/Phone'

import useClientService from '../../common/useClientService'

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 'auto',
    width: '480px'
  },
  guestRegistration: {
    padding: theme.spacing(2)
  },
  registrationForm: {
    display: 'flex',
    flexDirection: 'column',

    '& > *': {
      marginTop: theme.spacing(2)
    }
  }
}))

const onChange = (f) => (e) => f(e.target.value)

const formatAsISODate = (date) => {
  const isoDateTimeString = date.toISOString()
  return isoDateTimeString.substring(0, isoDateTimeString.indexOf('T'))
}

const formatAsISOTime = (date) => {
  const isoDateTimeString = date.toISOString()
  return isoDateTimeString.substring(
    isoDateTimeString.indexOf('T') + 1,
    isoDateTimeString.indexOf('T') + 9
  )
}

export default () => {
  const clientService = useClientService()

  const restaurantName = 'A great place to eat!'
  const [lastname, setLastname] = useState('')
  const [firstname, setFirstname] = useState('')
  const [phone, setPhone] = useState('')
  const [tableNumber, setTableNumber] = useState('')

  const now = new Date()
  const isoTimeString = formatAsISOTime(now)
  const [date, setDate] = useState(formatAsISODate(now))
  const [from, setFrom] = useState(isoTimeString)
  const [to, setTo] = useState(isoTimeString)

  const onSubmit = (event) => {
    clientService.registerGuest({
      firstname,
      lastname,
      phone,
      tableNumber,
      date,
      from,
      to
    })

    event.preventDefault()
  }

  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Paper>
        <div className={classes.guestRegistration}>
          <Typography variant="h5" noWrap>
            {restaurantName} - Guest registration
          </Typography>
          <form onSubmit={onSubmit}>
            <div className={classes.registrationForm}>
              <TextField
                fullWidth
                required
                autoFocus
                name="lastname"
                placeholder="Lastname"
                label="Lastname"
                size="small"
                value={lastname}
                onChange={onChange(setLastname)}
              />
              <TextField
                fullWidth
                required
                name="firstname"
                placeholder="Firstname"
                label="Firstname"
                size="small"
                value={firstname}
                onChange={onChange(setFirstname)}
              />
              <TextField
                fullWidth
                required
                name="phone"
                placeholder="Phone"
                label="Phone"
                size="small"
                value={phone}
                onChange={onChange(setPhone)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <PhoneIcon />
                    </InputAdornment>
                  )
                }}
              />
              <TextField
                fullWidth
                required
                name="tableNumber"
                placeholder="Table Number"
                label="Table Number"
                size="small"
                value={tableNumber}
                onChange={onChange(setTableNumber)}
              />
              <TextField
                fullWidth
                required
                type="date"
                name="date"
                label="Date of Visit"
                size="small"
                value={date}
                onChange={onChange(setDate)}
              />
              <TextField
                fullWidth
                required
                type="time"
                name="from"
                label="From"
                size="small"
                value={from}
                onChange={onChange(setFrom)}
              />
              <TextField
                fullWidth
                required
                type="time"
                name="to"
                label="To"
                size="small"
                value={to}
                onChange={onChange(setTo)}
              />

              <Button type="submit" color="inherit">
                Register
              </Button>
            </div>
          </form>
        </div>
      </Paper>
    </div>
  )
}
```

I have introduced two convenience methods to format the current date as [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) date and time. This is the format that is expected by the date and time fields.

```js
// <-- snip -->
const formatAsISODate = (date) => {
  const isoDateTimeString = date.toISOString()
  return isoDateTimeString.substring(0, isoDateTimeString.indexOf('T'))
}

const formatAsISOTime = (date) => {
  const isoDateTimeString = date.toISOString()
  return isoDateTimeString.substring(
    isoDateTimeString.indexOf('T') + 1,
    isoDateTimeString.indexOf('T') + 9
  )
}
// <-- snap -->
```

Then I have added some more state variables to capture the input of the user.

```jsx
// <-- snip -->
const [phone, setPhone] = useState('')
const [tableNumber, setTableNumber] = useState('')

const now = new Date()
const isoTimeString = formatAsISOTime(now)
const [date, setDate] = useState(formatAsISODate(now))
const [from, setFrom] = useState(isoTimeString)
const [to, setTo] = useState(isoTimeString)
// <-- snap -->
```

I have also added those state variables to the submit function. In addition I have changed the function to use a object instead of arguments as this has some advantages regarding readability and extensibility.

```js
// <-- snip -->
const onSubmit = (event) => {
  clientService.registerGuest({
    firstname,
    lastname,
    phone,
    tableNumber,
    date,
    from,
    to
  })

  event.preventDefault()
}
// <-- snap -->
```

Consequently, the client service also now needs to consume the object.

```js
export default {
  registerGuest: async (registerInformation) => {
    const {
      firstname,
      lastname,
      phone,
      tableNumber,
      date,
      from,
      to
    } = registerInformation
    console.log(
      `Registering guest ${firstname}, ${lastname}, ${phone}, ${tableNumber}, ${date}, ${from}, ${to}`
    )
  }
}
```

All other changes just involve showing some text fields to get the input from the user.

The last thing I have changed is, to provide a nicer headline and add the restaurant name as a constant. This is a preparation to make this configurable in the future.

```jsx
// <-- snip -->
const restaurantName = 'A great place to eat!'
// <-- snap -->

// <-- snip -->
return (
// <-- snip -->
  <Typography variant="h5" noWrap>
    {restaurantName} - Guest registration
  </Typography>
// <-- snap -->
)
// <-- snap -->
```

The final version of the registration form looks like this:

![]()

Let's now focus on the backend service and start with setting it up.

## Setting up the backend service

At first I thought about using [Google Firebase](https://firebase.google.com/) for the backend as I already used it a lot. But I was also playing a lot with  [ASP.NET Core](https://docs.microsoft.com/de-de/aspnet/core/?view=aspnetcore-3.1) lately, so I decide to go with that.