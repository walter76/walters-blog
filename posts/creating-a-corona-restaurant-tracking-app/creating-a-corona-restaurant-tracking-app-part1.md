I don't know about your country, but since Germany got out of the full contact
restrictions there are some rules to be followed. They have been put into place
to control the pandemic and prevent a real lockdown. One of those rules is, that
whenever you go to a restaurant, you have to leave your name and phone number.
In case somebody is tested positive for Covid-19 later on, the authorities can
track possible contacts and do localized testing to contain the further spread
of the pandemic.

Since this rule has been put into place, I have been wondering how far behind
Germany is in terms of digitalization. Whenever you go into a restaurant, you
will get a sheet of paper and a pen where you write down your contact
information. There are exceptions where this is done electronically with a
mobile app, but my experience is, that this is a rare case.

So, this pen and paper (no, we are not doing a roleplaying game ;-)) has several
downsides:

* The pen and paper is touched by you and others and therefore it can support
  spreading the pandemic. Some might clean it, but to be honest I have rarely
  seen this.

* It is paper and the restaurant needs to archive it somewhere.

* It is paper and likely to get lost.

* When properly handled to track down infections later on, somebody should
  digitalize it. This means typing the information into some application (most
  likely Excel :-)).

* The process is error prone and makes it hard to track down people in case of
  an infection.

Having an app where you just type in the URI in your smart phone (or scan an QR
code) to enter your contact details is much simpler and makes it easier tracking
down people later on. It is also less error prone. For sure you need to deal
with the data privacy issues, but I think this can be managed.

Now that I have complained about how far behind we are in terms of
digitalization and as I am interested in technology and software development I
thought about creating a simple Corona restaurant tracking app. This project
will not fit into one article, so this is a start of a series.

I am aware that there are already app solutions for this problem, but I am
interested in how I would realize such a project. In addition I am going to put
the source code under MIT license on [gitlab](https://gitlab.com/walter76/guest-tracker)
for everyone to benefit from it. If I can help someone to realize a low-cost
solution I would be very happy! Also, if you plan to use my source code and
stuck, drop me an e-mail ([walter.blog.76(at)gmail.com](mailto:walter.blog.76@gmail.com)).
I am happy to help!

If you plan to follow along, you can find the code on [gitlab](https://gitlab.com)
as [guest-tracker](https://gitlab.com/walter76/guest-tracker).

# Requirements

As always I am going to do this in an incremental manner. The goal will be to
have a minimum viable product in the end. The simplest useful thing to have
would be:

* A registration form for guests to enter there personal data and table number
* A tabular report at which day and time guests were at the restaurant

To make the initial product less complex, I will hard-code some things that
should be configurable in a real-world multi-tenancy application. But I will try
to keep a usage by different customers in mind and prepare a little bit already.
This should also make it easier for somebody else to adapt the code to their
needs.

I have quickly sketched out some UI with my favorite drawing tool [draw.io](https://draw.io).

![](./01_mockup-registration-form.png)

The picture above shows the registration form. The next one shows a rough sketch
of the result table.

![](./02_mockup-result-table.png)

I am not very good at graphics, so it is really a rough sketch. Most likely the
UI will change during the process of development anyways.

# Setting up the project

Starting a new React project is always the same process. I will fire up a
console and enter:

```sh
npx create-react-app guest-tracker
```

This command is going to scaffold a new React project in the folder
`guest-tracker`. On my MacBook with [iTerm2](https://www.iterm2.com/) it looks
like this after it has finished.

![](./03_create-react-app.png)

Now I need to do some clean-up work for the project. Opening up Visual Studio
Code, the folder currently looks like this:

![](./04_vscode-initial-project.png)

I will use [React Material-UI](https://material-ui.com/) for the UI, so I do not
need CSS. I also don't want tests right now. Let's get rid of the following
files: `App,css`, `App.test.js`, `index.css`, `logo.svg` and `setupTests.js`.
Afterwards the directory looks like this:

![](./05_vscode-remove-files.png)

Next I remove everything from `App.js` and replace it with the following code.
This step is just to get a clean start.

```jsx
import React from 'react'

export default () => <div>guest-tracker-app</div>
```

And finally, I remove the import of `index.css` from `index.js` and do some
formatting.

```jsx
import React from 'react'
import ReactDOM from 'react-dom'

import App from './App'
import * as serviceWorker from './serviceWorker'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
```

Next, I switch to the folder `public` and `index.html`. I need to change the
meta tag for the description and the title.

```html
<!-- snip -->
<meta
  name="description"
  content="simple app to track down guests attending an event"
/>
<!-- snap -->

<!-- snip -->
<title>guest-tracker</title>
<!-- snap -->
```

And finally I have to adapt `manifest.json`.

```json
// snip
"short_name": "guest-tracker",
"name": "guest-tracker",
// snap
```

Now it is time to fire up the app for the first time and check if I made any
mistakes during cleaning up. After entering `yarn start` in the console I get
the following screen in the browser window.

![](./06_start-after-cleanup.png)

Looks like everything went well. Moving on.

## Google Material UI

All the apps I have created lately make use of [Google Material UI](https://material.io/).
It looks nice and there a lot of tools and libraries around.

As I am not that good with design and colors, I am very grateful that Google
provides some tools that help me out. Personally I like the [Material palette generator](https://material.io/design/color/the-color-system.html#tools-for-picking-colors)
very much. You just need to select a primary and secondary color. Then you can
check it out in the Color Tool with a simple click. For this application I have
chosen a green as a primary and orange as a secondary color.

![](./07_material-io-palette-generator.png)

Next, I click on the link in the lower right corner *View in color tool*. This
brings me to the color tool which provides more flexibility but is also more
complex. Sometimes I tweak the colors, this time I keep it as it is.

![](./08_material-io-color-tool.png)

The color tool has another nice feature. By clicking on the button that looks
like a chain in the upper right, you can permanently save your selection. An URI
is copied to my clipboard after clicking the button.

The next step is to pull the libraries for [material-ui](https://material-ui.com/)
and make the rough setup. I switch to the project directory and add two
libraries with [yarn](https://yarnpkg.com/).

```sh
yarn add @material-ui/core @material-ui/icons
```

Back in the IDE, I create a new folder `app` in the folder `src`. Inside I put a
new file `theme.js`.

```js
import { red } from '@material-ui/core/colors'
import { createMuiTheme } from '@material-ui/core/styles'

// https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=008e00&secondary.color=f9a000

export default createMuiTheme({
  palette: {
    primary: {
      light: '#50bf3e',
      main: '#008e00',
      dark: '#005f00',
      contrastText: '#000'
    },
    secondary: {
      light: '#ffd148',
      main: '#f9a000',
      dark: '#c07100',
      contrastText: '#000'
    },
    error: {
      main: red.A400
    },
    background: {
      default: '#fff'
    }
  }
})
```

I have copied the link from the color tool as a comment inside the file. What I
then did is, copy the color definitions from the color tool to the theme.
Besides that I defined a error-color in `red.A400`. Not sure yet if this will
work out from a design perspective, but I can also easily change this later on.

Now that I have created a material color theme, I edit `src/index.js` to use it.

```jsx
// <-- snip -->
import CssBaseline from '@material-ui/core/CssBaseline'
import { ThemeProvider } from '@material-ui/core/styles'

import theme from './app/theme'
// <-- snap -->

// <-- snip -->
ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
// <-- snap -->
```

I have added three imports, two for material-ui: `CssBaseline` and 
`ThemeProvider` and `theme` from the file we just created.

```jsx
import CssBaseline from '@material-ui/core/CssBaseline'
import { ThemeProvider } from '@material-ui/core/styles'

import theme from './app/theme'
```

Now I changed the rendered JSX to apply the CSS baseline with `<CssBaseline />`
and wrapped it in the `<ThemeProvider>` to apply the material theme.

```jsx
<ThemeProvider theme={theme}>
  <CssBaseline />
  <App />
</ThemeProvider>
```

There is one last thing missing. Material-ui uses the [Roboto font](https://fonts.google.com/specimen/Roboto)
from Google. We need to reference it in `public/index.html`. So I add the
following line above the `<title>` tag:

```html
<!-- snip -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
<!-- snap -->
```

I will now start the application to check if everything is working. From the UI
perspective there is not much yet to see from material-ui. If you look close,
you recognize that the margin is put hard to the left and top and the font looks
a little bit different.

# Summary

That's it for the first part of the series. Today, I have described my idea and
why I am going to build a Corona restaurant tracking app. After defining some
requirements for an MVP I have setup the project and installed the basics for
the UI.

In the next post, I will build the form for registration of an guest.

Again, if you want to follow along, check out the repo at [gitlab](https://gitlab.com/walter76/guest-tracker).
The code will be a little bit ahead of the posts, so don't be surprised if there
is already some basis for the registration form.