# Covid-19: My two cents

There are so many articles around about Covid-19 and initially I did not want to jump onto that train and write my own. So here it is, [my two cents](https://en.wikipedia.org/wiki/My_two_cents) on Covid-19.

Before going into the details, I would like to tell you why I decided to write this article. Somebody that is close to me started to question the whole Covid-19 thing. At first it was small things, but lately I have been watching while this person is being dragged more and more into theories about conspiracies. This got me started thinking. Why are so many people that I consider as sound and intelligent all of a sudden ignore scientific proof, data and e.g. question that wearing masks is a good measure to stop the spread of the Covid-19 pandemic.

An additional question that I have been thinking about is, why is it so easy for "false prophets" to spread wrong information? What makes it so hard to use facts in the discussion?

My hypothesis is based on three major factors:

1. The Covid-19 pandemic as a chaotic problem.
2. The world never faced a pandemic before.
3. Fear.

Those three factors make it easy to take every scientific paper or data and use it for your own purpose. Let's now look at those factors in detail.

## Covid-19 pandemic is a chaotic problem



# Notes / Outline

Write article on Covid-19 and why there are so many "false prophets". Outline: Two factors: It is a chaotic problem (analogy to types of problems in SE). Nobody knows because of so many factors influencing each other. Secondly: fear. People react different on fear: some curl up in a corner, others fight, others clinch to each hint that seems an easy way out. In total this makes it easy for all kind of "false prophets" to get there thing going. Basically for every fact you can find counter-facts.

