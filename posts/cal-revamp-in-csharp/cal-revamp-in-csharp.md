There is a lot to learn by just reimplementing already existing commandline
tools. Especially those originating from Unix. So, I have reimplemented `cal`
because ... I wanted to.

In the end I learned a lot about the usage of `DateTime` in .NET C# and how to
manipulate console colors. And I also had a lot of fun coding this.

`cal` is a command line tool that you can use to display the calendar on the
console. The following screenshot shows what it looks like in the Ubuntu Linux
subsystem running on Windows.

![](./01_running_cal.png)

Running it without any further arguments just displays the calendar of the
current month and highlights today. There are more options and you can
check them in the [man pages](https://www.man7.org/linux/man-pages/man1/cal.1.html).
The tool I have implemented will not support any functionality besides the
default behavior.

# Setting up the .NET Core project

Let's get started right and away and as a first step, create a .NET Core project
for `cal`.

If you want to follow along, I am using [Visual Studio Code](https://code.visualstudio.com/)
as a IDE with the [Microsoft C# extension](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp)
installed. In addition I am making use of the command line and `dotnet` CLI. If
you are familiar with another IDE, e. g. Visual Studio, Rider or even Vim, it
will be fine.

For starters, create a new .NET Core project by opening a command line, navigate
to some directory where you usually have your source code and enter:

```sh
mkdir cal-revamp
cd cal-revamp
dotnet new console
```

This will scaffold a new console project in the folder `cal-revamp`. After the
commands have finished, you should see something like this:

![](./02_create_core_project.png)

Let's check if everything is set up, by entering `dotnet run` in the console.
You should see `Hello World!` which is the default implementation for a newly
created .NET core project.

We will work in small increments and adding functionality after functionality. I
am always using a [git](https://git-scm.com/)-repo to keep track of my changes.
If you want to do the same, just go ahead and create one with `git init` in the
folder `cal-revamp`.

# Printing the month and year

Looking again at the output of `cal` we see, that the first thing to do is to
print the month and year as the first line of output.

```csharp
using System;
using System.Globalization;

namespace cal_revamp
{
    class Program
    {
        static void Main(string[] args)
        {
            var today = DateTime.Now;
            var enCulture = CultureInfo.CreateSpecificCulture("en-US");

            Console.WriteLine(today.ToString("MMMM yyyy", enCulture));
        }
    }
}
```

For later reference we store the current date into a variable `today` and then
print the month and year to the console by using the `ToString`-method of
`today`. As the default culture of my system is `de-DE` I had to create a
English culture to make the month names displayed in English. This culture is
then passed to the `ToString`-method.

```csharp
today.ToString("MMMM yyyy", enCulture)
```

The string parameter `"MMMM yyyy"` is responsible for printing the full name of
the month followed by a space and the year in four digits.

So, we have the month and year as the first line of output. Not yet pretty as it
is not centered, but it is there.

# Printing the day numbers

Let us now go ahead and print the day numbers for the whole month. Again, we
start simple. Add the following code at the end of the `Main`-method.

```csharp
var totalDaysOfMonthList = new int[] {
    31,
    28,
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31
};
var totalDaysOfMonth = totalDaysOfMonthList[today.Month - 1];

for (var dayOfMonth = 1; dayOfMonth <= totalDaysOfMonth; dayOfMonth++)
{
    Console.Write($"{dayOfMonth, 2} ");
}
```

I have not come up with a better idea to calculate the total number of days for
the current month, so I just create an array `totalDaysOfMonthList`with all the
numbers. Then, I get the number for the current month with the following code.

```csharp
var totalDaysOfMonth = totalDaysOfMonthList[today.Month - 1];
```

Months are enumerated starting from 1 in C#, therefore we need to subtract 1
from `today.Month` to get the correct index into our array. Afterwards we print
the numbers and make it even a little bit pretty by aligning them to the right
with two digits with `$"{dayOfMonth, 2} ".

Let's run the program with `dotnet run` and check the output.

![](./03_print_days_first_run.png)

Ok, this gives us all the days in a month. Again, not yet pretty formatted and
there is a problem regarding the leap year, but for a first try it is fine.

## Fixing the leap year issue

Let's try something out and not take the current date, but set the date to
February 2020.

```csharp
// <-- snip
var today = new DateTime(2020, 2, 1);
// snap -->
```

Run the program with `dotnet run` and check the output.

![](./04_leap_year_issue.png)

So, this is not what we would have expected. The year 2020 clearly is a leap
year and therefore we should see 29 days and not 28. Let's fix this by adding
the following lines of code below the calculation of `totalDaysOfMonth`.

```csharp
// <-- snip
if (today.Month == 2 && DateTime.IsLeapYear(today.Year)) {
    totalDaysOfMonth = 29;
}
// snap -->
```

Let's run the program again and check the output.

![](./05_leap_year_fixed.png)

Looks about right, now we have 29 days. Remove our temporary test code from the
beginning.

```csharp
// <-- snip
var today = DateTime.Now;
// snap -->
```

## Wrapping lines

If we look at the output, we can see, that there is a line break every 7 days to
separate the weeks. Again, let's start simple and think how we can achieve this.

In situations where you have a `for`-loop with an index and need to do something
different every n-th run it is usually the modulo-operator `%` coming to your
rescue. This case is nothing different. Change the code of the `for`-loop to
this.

```csharp
// <-- snip
for (var dayOfMonth = 1; dayOfMonth <= totalDaysOfMonth; dayOfMonth++)
{
    Console.Write($"{dayOfMonth, 2}");

    if ((dayOfMonth % 7) == 0) {
        Console.WriteLine();
    } else {
        Console.Write(" ");
    }
}
// snap -->
```

We now have a line break every 7th run in the `for`-loop otherwise we write an
additional space to separate the day numbers. Please note that I have also
removed the additional space ' ' in the first `Console.Write($"{dayOfMonth, 2}");`. 

Run the program. You should see something like this.

![](./06_wrap_weeks.png)

In total the code for our reimplementation of `cal` should now look like this.

```csharp
using System;
using System.Globalization;

namespace cal_revamp
{
    class Program
    {
        static void Main(string[] args)
        {
            var today = DateTime.Now;
            var enCulture = CultureInfo.CreateSpecificCulture("en-US");

            Console.WriteLine(today.ToString("MMMM yyyy", enCulture));

            var totalDaysOfMonthList = new int[] {
                31,
                28,
                31,
                30,
                31,
                30,
                31,
                31,
                30,
                31,
                30,
                31
            };
            var totalDaysOfMonth = totalDaysOfMonthList[today.Month - 1];
            if (today.Month == 2 && DateTime.IsLeapYear(today.Year)) {
                totalDaysOfMonth = 29;
            }

            for (var dayOfMonth = 1; dayOfMonth <= totalDaysOfMonth; dayOfMonth++)
            {
                Console.Write($"{dayOfMonth, 2}");

                if ((dayOfMonth % 7) == 0) {
                    Console.WriteLine();
                } else {
                    Console.Write(" ");
                }
            }
        }
    }
}
```

# Printing day names

The next big thing would be to have a header with the short form of the week
days to make it look like a real calendar. We start by just adding a simple
header. Add the next lines of C# code below our current header.

```csharp
// <-- snip
Console.WriteLine();
Console.WriteLine("Su Mo Tu We Th Fr Sa");
// snap -->
```

Ok, we now have a header, but it is not yet right. The first of May was not a
Sunday but a Friday. Let's fix this.

If we look at `cal` again, we can see that the first line is offset to have the
first day number aligned below the right name. We can also calculate this if we
take the number of the day of the week and multiply it by 3. Luckily there is a
C# property `DayOfWeek` that will help us there. `DayOfWeek` gives an integer
for every day of the week indicating the day name. If we use this for the first
date of the current month, we get the number we have to multiply by 3.

Add the following code right before our `for`-loop that prints the day numbers.

```csharp
// <-- snip
var monthStartDate = new DateTime(today.Year, today.Month, 1);
var startingDayOffset = (int) monthStartDate.DayOfWeek;
var leadingSpaces = new string(' ', startingDayOffset * 3);
Console.Write(leadingSpaces);
// snap -->
```

This will offset the first day number just about right, but the line breaks will
be screwed up. To fix this, we need to change the condition of  our
`if`-statement that is handling the line breaks to take the offset into account.

```csharp
// <-- snip
if (((dayOfMonth + startingDayOffset) % 7) == 0) {
// snap -->
```

Run our program. It should now prettily print the day names and the numbers
should be aligned.

![](./07_print_day_names.png)

By now, our code should look like this.

```csharp
using System;
using System.Globalization;

namespace cal_revamp
{
    class Program
    {
        static void Main(string[] args)
        {
            var today = DateTime.Now;
            var enCulture = CultureInfo.CreateSpecificCulture("en-US");

            Console.WriteLine(today.ToString("MMMM yyyy", enCulture));
            Console.WriteLine();
            Console.WriteLine("Su Mo Tu We Th Fr Sa");

            var totalDaysOfMonthList = new int[] {
                31,
                28,
                31,
                30,
                31,
                30,
                31,
                31,
                30,
                31,
                30,
                31
            };
            var totalDaysOfMonth = totalDaysOfMonthList[today.Month - 1];
            if (today.Month == 2 && DateTime.IsLeapYear(today.Year)) {
                totalDaysOfMonth = 29;
            }

            var monthStartDate = new DateTime(today.Year, today.Month, 1);
            var startingDayOffset = (int) monthStartDate.DayOfWeek;
            var leadingSpaces = new string(' ', startingDayOffset * 3);
            Console.Write(leadingSpaces);

            for (var dayOfMonth = 1; dayOfMonth <= totalDaysOfMonth; dayOfMonth++)
            {
                Console.Write($"{dayOfMonth, 2}");

                if (((dayOfMonth + startingDayOffset) % 7) == 0) {
                    Console.WriteLine();
                } else {
                    Console.Write(" ");
                }
            }
        }
    }
}
```

# Highlighting the current date

There is not much left for a first running version of `cal`. The next thing on
the list is to highlight the current date.

As our program is running on the console it is possible to change the color with
the right [ANSI escape codes](https://en.wikipedia.org/wiki/ANSI_escape_code).
They are an relict from video text terminals and terminal emulators but are
still supported even in MS Windows console shells. The escape codes we will be
using are.

| ANSI code   | Description                |
| ----------- | -------------------------- |
| `ESC[1;47m` | set white background color |
| `ESC[1;30m` | set black foreground color |
| `ESC[0m`    | reset colors to default    |

In C# you can use ANSI escape codes in `System.Console.WriteLine` by `\x1B `
sequence, e. g. use `\x1B[47m` to set the background color to white. Let's put
this new knowledge into action. Change the code in the `if`-statement where we
write the day number.

```csharp
// <-- snip
if (dayOfMonth == today.Day)
{
    Console.Write($"\x1B[47m\x1B[30m{dayOfMonth, 2}\x1B[0m");
} else {
    Console.Write($"{dayOfMonth, 2}");
}
// snap -->
```

If you now run the program you should get something like this.

![](./08_highlight_current.png)

Neat! Almost done.

# Finishing up

One last thing is left: we should neatly center our header. For that, change the
code at the beginning to.

```csharp
// <-- snip
var monthYearHeader = today.ToString("MMMM yyyy", enCulture);
var dayNamesHeader = "Su Mo Tu We Th Fr Sa";
var monthYearHeaderOffset =
    (dayNamesHeader.Length - monthYearHeader.Length) / 2;
var leadingSpacesMonthYearHeader =
    new string(' ', monthYearHeaderOffset);

Console.WriteLine($"{leadingSpacesMonthYearHeader}{monthYearHeader}");
Console.WriteLine(dayNamesHeader);
// snap -->
```

We have extracted the month/year and day header in the variables
`monthYearHeader` and `dayNamesHeader`. Then we subtracted the length of
`monthYearHeader` from `dayNamesHeader`, divided it by 2 to calculate the number
of leading spaces we need to add to center the header. We have also removed the
extra line break between the month/year and day header.

That's it. A quick rewrite of the core functionality of `cal` in .NET C#. If you
are interested, you can find the complete source code as a snippet on [GitLab](https://gitlabl.com)
as [Unix-like cal](https://gitlab.com/snippets/1982772).
