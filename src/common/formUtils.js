export default (f) => (e) => f(e.target.value)
