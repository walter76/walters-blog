import { useState, useEffect } from 'react'

import useFirebase from './useFirebase'

export default () => {
  const firebase = useFirebase()
  const [authUser, setAuthUser] = useState(null)

  useEffect(() => {
    const authStateChangedListener = firebase.auth.onAuthStateChanged(user => {
      if (user) {
        setAuthUser(user)
      } else {
        setAuthUser(null)
      }
    })

    return () => authStateChangedListener()
  })

  return authUser
}
