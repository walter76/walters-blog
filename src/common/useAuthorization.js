import { useEffect } from 'react'
import { useHistory } from 'react-router-dom'

import useAuthUser from './useAuthUser'
import useFirebase from './useFirebase'

import * as ROUTES from './routes'

export default condition => {
  const firebase = useFirebase()
  const authUser = useAuthUser()
  const history = useHistory()

  useEffect(() => {
    const authStateChangedListener = firebase.auth.onAuthStateChanged(user => {
      if (!condition(user)) {
        history.push(ROUTES.SIGN_IN)
      }
    })

    return () => authStateChangedListener()
  })

  return condition(authUser)
}
