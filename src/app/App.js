import React, { useState } from "react";
import clsx from "clsx";
import {
  BrowserRouter as Router,
  Route,
  Link as RouterLink,
} from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import {
  Toolbar,
  Typography,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  IconButton,
  AppBar,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import DescriptionIcon from "@material-ui/icons/Description";
import InfoIcon from "@material-ui/icons/Info";
import WarningIcon from "@material-ui/icons/Warning";
import CreateNewFolderIcon from "@material-ui/icons/CreateNewFolder";

import useAuthUser from "../common/useAuthUser";

import * as ROUTES from "../common/routes";

import Landing from "../features/landing/Landing";
import About from "../features/about/About";
import Disclaimer from "../features/disclaimer/Disclaimer";
import SignIn from "../features/signIn/SignIn";
import SignOut from "../features/signOut/SignOut";
import ArticlesOverview from "../features/articlesOverview/ArticlesOverview";

import ArticleEdit from "../features/articles/ArticleEdit";
import ArticleView from "../features/articles/ArticleView";
import ArticleCreate from "../features/articles/ArticleCreate";

const drawerWidth = 200;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor: theme.palette.primary.dark,
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easingOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: theme.palette.primary.main,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
  menuText: {
    color: theme.palette.secondary.main,
  }
}));

export default () => {
  const authUser = useAuthUser();
  const isAuthenticated = !!authUser;

  const [isMenuOpen, setMenuOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMenuOpen(!isMenuOpen);
  };

  const classes = useStyles();

  return (
    <Router>
      <div className={classes.root}>
        <AppBar
          className={clsx(classes.appBar, {
            [classes.appBarShift]: isMenuOpen,
          })}
          position="fixed"
        >
          <Toolbar>
            <IconButton
              color="inherit"
              edge="start"
              onClick={handleDrawerToggle}
              className={clsx(classes.menuButton, isMenuOpen && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap>
              Walter's Coding Refuge
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          classes={{ paper: classes.drawerPaper }}
          anchor="left"
          open={isMenuOpen}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={handleDrawerToggle}>
              <ChevronLeftIcon color="secondary" />
            </IconButton>
          </div>
          <List component="nav">
            <ListItem button component={RouterLink} to={ROUTES.ARTICLES}>
              <ListItemIcon>
                <DescriptionIcon color="secondary" />
              </ListItemIcon>
              <ListItemText className={classes.menuText} primary="articles" />
            </ListItem>
            <ListItem button component={RouterLink} to={ROUTES.ABOUT}>
              <ListItemIcon>
                <InfoIcon color="secondary" />
              </ListItemIcon>
              <ListItemText className={classes.menuText} primary="about" />
            </ListItem>
            <ListItem button component={RouterLink} to={ROUTES.DISCLAIMER}>
              <ListItemIcon>
                <WarningIcon color="secondary" />
              </ListItemIcon>
              <ListItemText className={classes.menuText} primary="disclaimer" />
            </ListItem>
            {isAuthenticated && (
              <>
                <ListItem button component={RouterLink} to={ROUTES.NEW}>
                  <ListItemIcon>
                    <CreateNewFolderIcon color="secondary" />
                  </ListItemIcon>
                  <ListItemText className={classes.menuText} primary="new" />
                </ListItem>
                <ListItem>
                  <SignOut />
                </ListItem>
              </>
            )}
          </List>
        </Drawer>

        <main
          className={clsx(classes.content, {
            [classes.contentShift]: isMenuOpen,
          })}
        >
          <div className={classes.toolbar} />
          <Route exact path={ROUTES.LANDING} component={Landing} />
          <Route path={ROUTES.ABOUT} component={About} />
          <Route path={ROUTES.DISCLAIMER} component={Disclaimer} />
          <Route path={ROUTES.SIGN_IN} component={SignIn} />
          <Route path={`${ROUTES.EDIT}/:articleId`} component={ArticleEdit} />
          <Route path={ROUTES.ARTICLES} component={ArticlesOverview} />
          <Route path={`${ROUTES.ARTICLE}/:slug`} component={ArticleView} />
          <Route path={ROUTES.NEW} component={ArticleCreate} />
        </main>
      </div>
    </Router>
  );
};
