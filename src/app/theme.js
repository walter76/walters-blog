import { red } from '@material-ui/core/colors'
import { createMuiTheme } from '@material-ui/core/styles'

// https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=263238&secondary.color=3F51B5

export default createMuiTheme({
  palette: {
    primary: {
      light: '#4f5b62',
      main: '#263238',
      dark: '#000a12',
      contrastText: '#fff'
    },
    secondary: {
      light: '#757de8',
      main: '#3f51b5',
      dark: '#002984',
      contrastText: '#fff'
    },
    error: {
      main: red.A400
    },
    background: {
      default: '#263238'
    }
  }
})
