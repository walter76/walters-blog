import { combineReducers } from 'redux'

import articleReducer from '../features/articles/articleReducer'
import articlesOverviewReducer from '../features/articlesOverview/articlesOverviewReducer'

export default combineReducers({
  article: articleReducer,
  articlesOverview: articlesOverviewReducer
})
