import React from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";

import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

import ArticleForm from "./ArticleForm";

import useAuthorization from "../../common/useAuthorization";
import userIsAuthenticated from "../../common/authRules";
import * as ROUTES from "../../common/routes";

import { createArticle } from "./articleActions";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    margin: "auto",
    color: theme.palette.primary.contrastText,
  },
}));

export default () => {
  useAuthorization(userIsAuthenticated);

  const dispatch = useDispatch();
  const history = useHistory();

  const saveArticle = (
    _id,
    slug,
    title,
    introduction,
    body,
    headerImageUri
  ) => {
    dispatch(createArticle(slug, title, introduction, body, headerImageUri));

    history.push(ROUTES.LANDING);
  };

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h5" noWrap>
        New article
      </Typography>
      <ArticleForm article={{}} saveArticle={saveArticle} />
    </div>
  );
};
