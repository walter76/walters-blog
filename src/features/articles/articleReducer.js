import * as TYPE from "./types";

const initState = { article: {} };

export default (state = initState, action) => {
  switch (action.type) {
    case TYPE.ARTICLE_FOUND: {
      return {
        ...state,
        article: action.article,
      };
    }

    default:
      return state;
  }
};
