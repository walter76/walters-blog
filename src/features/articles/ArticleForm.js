import React, { useState } from "react";

import { makeStyles } from "@material-ui/core/styles";
import { TextField, Button } from "@material-ui/core";

import onChange from "../../common/formUtils";

const useStyles = makeStyles((theme) => ({
  inputForm: {
    display: "flex",
    flexDirection: "column",

    "& > *": {
      marginTop: theme.spacing(2),
    },
  },
}));

export default ({ article, saveArticle }) => {
  const [slug, setSlug] = useState(article.slug);
  const [title, setTitle] = useState(article.title);
  const [headerImageUri, setHeaderImageUri] = useState(article.headerImageUri);
  const [introduction, setIntroduction] = useState(article.introduction);
  const [body, setBody] = useState(article.body);

  const onSubmit = (event) => {
    saveArticle(article.id, slug, title, introduction, body, headerImageUri);

    event.preventDefault();
  };

  const classes = useStyles();

  return (
    <form className={classes.inputForm} onSubmit={onSubmit}>
      <TextField
        fullWidth
        required
        autoFocus
        name="slug"
        placeholder="Slug"
        size="small"
        variant="outlined"
        value={slug}
        onChange={onChange(setSlug)}
        color="secondary"
      />
      <TextField
        fullWidth
        required
        name="title"
        placeholder="Title"
        size="small"
        variant="outlined"
        value={title}
        onChange={onChange(setTitle)}
        color="secondary"
      />
      <TextField
        fullWidth
        required
        name="headerImageUri"
        placeholder="Header Image URI"
        size="small"
        variant="outlined"
        value={headerImageUri}
        onChange={onChange(setHeaderImageUri)}
        color="secondary"
      />
      <TextField
        fullWidth
        required
        multiline
        rows="10"
        name="introduction"
        placeholder="Introduction"
        size="small"
        variant="outlined"
        value={introduction}
        onChange={onChange(setIntroduction)}
        color="secondary"
      />
      <TextField
        fullWidth
        required
        multiline
        rows="40"
        name="body"
        placeholder="Body"
        size="small"
        variant="outlined"
        value={body}
        onChange={onChange(setBody)}
        color="secondary"
      />
      <Button type="submit" color="inherit" variant="outlined">
        Save article
      </Button>
    </form>
  );
};
