import React, { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";

import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

import ArticleForm from "./ArticleForm";

import useFirebase from "../../common/useFirebase";
import useAuthorization from "../../common/useAuthorization";
import userIsAuthenticated from "../../common/authRules";
import * as ROUTES from "../../common/routes";

import { updateArticle } from "./articleActions";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    margin: "auto",
    color: theme.palette.primary.contrastText,
  },
}));

export default () => {
  useAuthorization(userIsAuthenticated);

  const dispatch = useDispatch();
  const { articleId } = useParams();
  const firebase = useFirebase();

  const [article, setArticle] = useState(null);

  useEffect(() => {
    const findArticleById = async () => {
      const articleDoc = await firebase.firestore
        .collection("articles")
        .doc(articleId)
        .get();

      if (!articleDoc.exists) {
        console.error(`article ${articleId} not found`);
      } else {
        const article = {
          id: articleDoc.id,
          ...articleDoc.data(),
        };

        setArticle(article);
      }
    };

    findArticleById(articleId);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const history = useHistory();

  const saveArticle = (id, slug, title, introduction, body, headerImageUri) => {
    dispatch(
      updateArticle(id, slug, title, introduction, body, headerImageUri)
    );

    history.push(ROUTES.LANDING);
  };

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h5" noWrap>
        Edit article
      </Typography>
      {article && <ArticleForm article={article} saveArticle={saveArticle} />}
    </div>
  );
};
