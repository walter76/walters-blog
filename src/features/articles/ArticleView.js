import React, { useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import { makeStyles } from "@material-ui/core/styles";
import { Typography, Button } from "@material-ui/core";

import Markdown from "../markdown/Markdown";
import useAuthUser from "../../common/useAuthUser";
import * as ROUTES from "../../common/routes";

import { findArticleBySlug } from "./articleActions";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    margin: "auto",
    color: theme.palette.primary.contrastText,
  },
  detailsContainer: {
    display: "flex",
    maxWidth: "450px",
    justifyContent: "space-between",
    color: theme.palette.primary.light,
  },
  headerImageContainer: {
    marginTop: theme.spacing(2),
    maxWidth: "800px",
    display: "flex",
    justifyContent: "center",
  },
  headerImage: {
    maxHeight: "250px",
    maxWidth: "100%",
  },
  articleBody: {
    marginTop: theme.spacing(2),
    maxWidth: "800px",
  },
}));

export default () => {
  const { slug } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(findArticleBySlug(slug));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const authUser = useAuthUser();
  const isAuthenticated = !!authUser;

  const article = useSelector((state) => state.article.article);

  const history = useHistory();

  const onEditButtonClicked = (e) => {
    history.push(`${ROUTES.EDIT}/${article.id}`);
  };

  const classes = useStyles();

  if (article.title === undefined) {
    return null;
  }

  return (
    <div className={classes.root}>
      <Typography variant="h5" noWrap>
        {article.title}
      </Typography>
      <div className={classes.detailsContainer}>
        <Typography variant="subtitle2">
          created: {article.created.toDate().toLocaleString()}
        </Typography>
        <Typography variant="subtitle2">
          last edited: {article.lastEdited.toDate().toLocaleString()}
        </Typography>
      </div>
      {article.headerImageUri && (
        <div className={classes.headerImageContainer}>
          <img
            className={classes.headerImage}
            src={article.headerImageUri}
            alt=""
          />
        </div>
      )}
      <div className={classes.articleBody}>
        <Markdown markdown={article.body} />
      </div>

      {isAuthenticated && (
        <Button color="secondary" onClick={onEditButtonClicked}>
          Edit
        </Button>
      )}
    </div>
  );
};
