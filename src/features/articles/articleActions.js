import * as TYPE from "./types";
import * as COMMON_TYPE from "../../common/commonTypes";

export const articleUpdated = (article) => ({
  type: COMMON_TYPE.ARTICLE_UPDATED,
  article,
});

export const updateArticle =
  (id, slug, title, introduction, body, headerImageUri) =>
  async (dispatch, getState, { firebase }) => {
    const articleDocRef = await firebase.firestore
      .collection("articles")
      .doc(id);

    await articleDocRef.update({
      slug,
      title,
      introduction,
      body,
      headerImageUri,
      lastEdited: firebase.now(),
    });

    const articleDoc = await articleDocRef.get();
    return dispatch(
      articleUpdated({ id: articleDoc.id, ...articleDoc.data() })
    );
  };

export const articleNotFound = {
  type: TYPE.ARTICLE_NOT_FOUND,
};

export const articleFound = (article) => ({
  type: TYPE.ARTICLE_FOUND,
  article,
});

export const findArticleBySlug =
  (slug) =>
  async (dispatch, getState, { firebase }) => {
    const articlesBySlugSnapshot = await firebase.firestore
      .collection("articles")
      .where("slug", "==", slug)
      .get();

    if (articlesBySlugSnapshot.empty) {
      return dispatch(articleNotFound);
    }

    const articleDoc = articlesBySlugSnapshot.docs[0];
    const article = {
      id: articleDoc.id,
      ...articleDoc.data(),
    };

    return dispatch(articleFound(article));
  };

export const articleCreated = (article) => ({
  type: COMMON_TYPE.ARTICLE_CREATED,
  article,
});

export const createArticle =
  (slug, title, introduction, body, headerImageUri) =>
  async (dispatch, getState, { firebase }) => {
    console.log(
      `new article: ${slug}, ${title}, ${headerImageUri}, ${introduction}, ${body}`
    );

    const createdArticleRef = await firebase.firestore
      .collection("articles")
      .add({
        slug,
        title,
        introduction,
        body,
        headerImageUri,
        lastEdited: firebase.now(),
        created: firebase.now(),
      });

    const articleDoc = await firebase.firestore
      .collection("articles")
      .doc(createdArticleRef.id)
      .get();

    const article = {
      id: articleDoc.id,
      ...articleDoc.data(),
    };

    return dispatch(articleCreated(article));
  };
