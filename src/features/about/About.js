import React from "react";
import walter from "./walter.jpg";
import logoGitLab from "./logo_gitlab.png";

import { makeStyles } from "@material-ui/core/styles";
import { Typography, Link, Button } from "@material-ui/core";
import { LinkedIn, Twitter, GitHub, Mail } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    margin: "auto",
    maxWidth: "600px",
    display: "flex",
    flexDirection: "column",
    color: theme.palette.primary.contrastText,
  },
  portraitContainer: {
    display: "flex",
    alignItems: "flex-end",
  },
  portrait: {
    border: "1px solid",
    borderColor: "grey",
    marginRight: theme.spacing(1),
  },
  caption: {
    maxWidth: "120px",
    color: theme.palette.primary.light,
  },
  heading: {
    marginTop: theme.spacing(3),
    paddingBottom: theme.spacing(2),
  },
  contactsContainer: {
    marginTop: theme.spacing(2),
    display: "flex",

    "& > *": {
      marginRight: theme.spacing(2),
    },
  },
}));

export default () => {
  const classes = useStyles();

  // todo: use material-ui icons for twitter, github, linkedin, gitlab?

  return (
    <div className={classes.root}>
      <div className={classes.portraitContainer}>
        <img src={walter} alt="Walter" className={classes.portrait} />

        <Typography variant="caption" className={classes.caption}>
          This is me in autumn 2019 right after I did cross the finishing line
          of my first half-marathon.
        </Typography>
      </div>

      <Typography variant="h6" noWrap className={classes.heading}>
        About Walter
      </Typography>

      <Typography paragraph>
        In my professional life, I am a Senior Software Architect for a big
        healthcare provider. I love creating things and above all I like
        computers and developing software. That is where my <i>second life</i>{" "}
        comes into the play.
      </Typography>

      <Typography paragraph>
        At my job, I try to do as much coding as possible, but the
        responsibilities I have been given do not allow me to create source code
        as much as I would love to. Also the type of software you create and the
        technologies you can choose from are limited in a big company. Therefore
        I spend my evenings and nights fiddling with all kinds of technology.
      </Typography>

      <Typography paragraph>
        Don't get me wrong. I love my job and right now, I could not imagine
        doing anything else. But there is a second-self that is still the little
        curious kid eager to learn how this brand new technology is working. ;-)
      </Typography>

      <Typography paragraph>
        There are two goals for this site:
        <ul>
          <li>
            I want to use it as a knowledge book for all the things that I learn
            in- and outside of my job.
          </li>
          <li>I want to share the content for others to learn.</li>
        </ul>
      </Typography>

      <Typography paragraph>
        Although I am a professional, I will fail during my learning adventures
        and might put things on the site that are not correct or free of error.
        Please point them out, because only if I see the errors I can learn from
        them. Besides, if you have a different opinion on the topic and want to
        discuss, you can also get in touch with me.
      </Typography>

      <Link href="mailto:walter.blog.76@gmail.com" underline="none">
        <Button
          variant="contained"
          color="primary"
          size="small"
          startIcon={<Mail />}
        >
          walter.blog.76(at)gmail.com
        </Button>
      </Link>

      <div className={classes.contactsContainer}>
        <Link
          href="https://www.linkedin.com/in/walter-stocker-28824b163"
          target="_blank"
          rel="noreferrer"
          underline="none"
        >
          <Button
            variant="contained"
            color="primary"
            size="small"
            startIcon={<LinkedIn />}
          >
            LinkedIn
          </Button>
        </Link>
        <Link
          href="https://twitter.com/Walter7610"
          target="_blank"
          rel="noreferrer"
          underline="none"
        >
          <Button
            variant="contained"
            color="primary"
            size="small"
            startIcon={<Twitter />}
          >
            Twitter
          </Button>
        </Link>
        <Link
          href="https://github.com/walter76"
          target="_blank"
          rel="noreferrer"
          underline="none"
        >
          <Button
            variant="contained"
            color="primary"
            size="small"
            startIcon={<GitHub />}
          >
            GitHub
          </Button>
        </Link>
        <Link
          href="https://gitlab.com/walter76"
          target="_blank"
          rel="noreferrer"
          underline="none"
        >
          <Button variant="contained" color="primary" size="small">
            <img src={logoGitLab} alt="GitLab" />
          </Button>
        </Link>
      </div>

      <Typography variant="h6" noWrap className={classes.heading}>
        About this site
      </Typography>

      <Typography paragraph>
        The complete source code of <b>Walter's Coding Refuge</b> can be found
        on{" "}
        <Link
          color="secondary"
          href="https://gitlab.com/walter76/walters-blog"
          target="_blank"
          rel="noreferrer"
        >
          GitLab
        </Link>
        .
      </Typography>
    </div>
  );
};
