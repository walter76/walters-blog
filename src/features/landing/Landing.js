import React from "react";
import { Link as RouterLink } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import { Typography, Link } from "@material-ui/core";

import * as ROUTES from "../../common/routes";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    maxWidth: "600px",
    margin: "auto",
    color: theme.palette.primary.contrastText,
  },
  heading: {
    paddingBottom: theme.spacing(2),
  },
}));

export default () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h6" noWrap className={classes.heading}>
        Use at your own risk!
      </Typography>

      <Typography paragraph>
        I love to code. Creating software has and probably will always be
        something that I could do day and night. Although my current job is
        awesome, I cannot code as much as I would like to.
      </Typography>

      <Typography paragraph>
        Welcome to my coding refuge. Almost all the articles on this site are
        related to coding and software craftmanship. They are also a brain-dump
        put into blog posts. I am using this site for my personal reference. If
        you are interested, read along, because I also love to share what I have
        learned on my coding journeys.
      </Typography>

      <Typography paragraph>
        Please be aware that I will not put much thought into writing posts with
        a pretty layout.
      </Typography>

      <Link color="secondary" component={RouterLink} to={ROUTES.ARTICLES}>
        Enter Walter's Coding Refuge ...
      </Link>
    </div>
  );
};
