import { isBefore, isAfter } from 'date-fns'

import * as TYPE from './types'
import * as COMMON_TYPE from '../../common/commonTypes'

const byDateDescending = (a, b) => {
  const lastEditedDateA = a.lastEdited.toDate()
  const lastEditedDateB = b.lastEdited.toDate()

  if (isBefore(lastEditedDateA, lastEditedDateB)) {
    return 1
  }

  if (isAfter(lastEditedDateA, lastEditedDateB)) {
    return -1
  }

  return 0
}

const initState = { articles: [] }

export default (state = initState, action) => {
  switch (action.type) {
    case TYPE.ARTICLES_OVERVIEW_LOADED: {
      const sortedArticlesList = action.articles.sort(byDateDescending)
      return { ...state, articles: sortedArticlesList }
    }

    case COMMON_TYPE.ARTICLE_UPDATED: {
      return {
        ...state,
        articles: state.articles.map((a) =>
          a.id === action.article.id ? action.article : a
        )
      }
    }

    case COMMON_TYPE.ARTICLE_CREATED: {
      const updatedArticlesList =
        [ action.article, ...state.articles ].sort(byDateDescending)
      return { ...state, articles: updatedArticlesList }
    }

    default:
      return state
  }
}
