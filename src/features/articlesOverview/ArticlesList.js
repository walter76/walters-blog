import React from 'react'

import ArticleIntro from './ArticleIntro'

export default ({ articles }) => {
  return (
    <>
      {articles &&
        articles.map((article) => (
          <ArticleIntro key={article.id} article={article} />
        ))}
    </>
  )
}
