import React from "react";
import { useHistory, Link as RouterLink } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import { Typography, Button, Link } from "@material-ui/core";

import Markdown from "../markdown/Markdown";
import useAuthUser from "../../common/useAuthUser";
import * as ROUTES from "../../common/routes";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    margin: "auto",
    color: theme.palette.primary.contrastText,
  },
  detailsContainer: {
    display: "flex",
    maxWidth: "450px",
    justifyContent: "space-between",
    color: theme.palette.primary.light,
  },
  articleBody: {
    marginTop: theme.spacing(2),
    maxWidth: "800px",
  },
}));

export default ({ article }) => {
  const authUser = useAuthUser();
  const isAuthenticated = !!authUser;

  const history = useHistory();

  const onEditButtonClicked = (e) => {
    history.push(`${ROUTES.EDIT}/${article.id}`);
  };

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h5" noWrap>
        {article.title}
      </Typography>
      <div className={classes.detailsContainer}>
        <Typography variant="subtitle2">
          created: {article.created.toDate().toLocaleString()}
        </Typography>
        <Typography variant="subtitle2">
          last edited: {article.lastEdited.toDate().toLocaleString()}
        </Typography>
      </div>
      <div className={classes.articleBody}>
        <Markdown markdown={article.introduction} />
      </div>
      <Link
        color="secondary"
        component={RouterLink}
        to={`${ROUTES.ARTICLE}/${article.slug}`}
      >
        Read more ...
      </Link>

      {isAuthenticated && (
        <Button onClick={onEditButtonClicked} color="secondary">
          Edit
        </Button>
      )}
    </div>
  );
};
