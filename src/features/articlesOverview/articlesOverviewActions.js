import * as TYPE from './types'

export const articlesOverviewLoaded = (articles) => ({
  type: TYPE.ARTICLES_OVERVIEW_LOADED,
  articles
})

export const loadArticlesOverview = () => async (
  dispatch,
  getState,
  { firebase }
) => {
  const articlesCollection = await firebase.firestore
    .collection('articles')
    .get()

  const allArticles = articlesCollection.docs.map((doc) => ({
    id: doc.id,
    ...doc.data()
  }))

  return dispatch(articlesOverviewLoaded(allArticles))
}
