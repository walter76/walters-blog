import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import ArticlesList from './ArticlesList'

import { loadArticlesOverview } from './articlesOverviewActions'

export default () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(loadArticlesOverview())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const articles = useSelector((state) => state.articlesOverview.articles)

  return <ArticlesList articles={articles} />
}
