import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'

import { Paper, Typography, TextField, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import * as ROUTES from '../../common/routes'
import onChange from '../../common/formUtils'
import useFirebase from '../../common/useFirebase'

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    marginTop: theme.spacing(2),
    marginRight: 'auto',
    marginBottom: theme.spacing(2),
    marginLeft: 'auto',
    maxWidth: 300,
  },
  inputForm: {
    display: 'flex',
    flexDirection: 'column',

    '& > *': {
      marginTop: theme.spacing(2),
    },
  },
}))

export default () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState(null)

  const firebase = useFirebase()
  const history = useHistory()

  const onSubmit = (event) => {
    firebase
      .signIn(email, password)
      .then(() => {
        setEmail('')
        setPassword('')
        setError(null)

        history.push(ROUTES.LANDING)
      })
      .catch((err) => {
        setError(err)
      })

    event.preventDefault()
  }

  const classes = useStyles()

  return (
    <Paper className={classes.root}>
      <Typography variant="h6" noWrap>
        Sign In
      </Typography>
      <form onSubmit={onSubmit} className={classes.inputForm}>
        <TextField
          fullWidth
          required
          autoFocus
          name="email"
          placeholder="E-Mail"
          size="small"
          variant="outlined"
          value={email}
          onChange={onChange(setEmail)}
        />
        <TextField
          fullWidth
          required
          name="password"
          placeholder="Password"
          type="password"
          size="small"
          variant="outlined"
          value={password}
          onChange={onChange(setPassword)}
        />

        {error && (
          <Typography variant="body1" color="error">
            {error.message}
          </Typography>
        )}

        <Button type="submit" color="inherit" variant="outlined">
          Sign In
        </Button>
      </form>
    </Paper>
  )
}
