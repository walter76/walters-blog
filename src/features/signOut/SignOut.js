import React from 'react'
import { useHistory } from 'react-router-dom'

import { Button } from '@material-ui/core'

import * as ROUTES from '../../common/routes'

import useFirebase from '../../common/useFirebase'

export default () => {
  const firebase = useFirebase()
  const history = useHistory()

  const signOut = (e) => {
    firebase.signOut().then(() => history.push(ROUTES.LANDING))
  }

  return <Button color="secondary" onClick={signOut}>Sign out</Button>
}
