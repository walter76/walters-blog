import React from "react";

import { Prism as SyntaxHighlighter } from "react-syntax-highlighter";
import { xonokai } from "react-syntax-highlighter/dist/esm/styles/prism";

const classes = {
  scaledImg: {
    maxWidth: "100%",
    maxHeight: "100%",
  },
};

const MarkdownCustomComponents = {
  code({ node, inline, className, children, ...props }) {
    const match = /language-(\w+)/.exec(className || "");

    return !inline && match ? (
      <SyntaxHighlighter
        style={xonokai}
        language={match[1]}
        PreTag="div"
        children={String(children).replace(/\n$/, "")}
        {...props}
      />
    ) : (
      <code className={className} {...props} />
    );
  },
  a: ({ title, children, ...props }) => (
    <a
      class="MuiTypography-root MuiLink-root MuiLink-underlineHover MuiTypography-colorSecondary"
      {...props}
    >
      {children}
    </a>
  ),
  img: ({ ...props }) => <img style={classes.scaledImg} alt="" {...props} />,
};

export default MarkdownCustomComponents;
