import React from "react";
import ReactMarkdown from "react-markdown";

import MarkdownCustomComponents from "./MarkdownCustomComponents";

export default ({ markdown }) => (
  <ReactMarkdown components={MarkdownCustomComponents} children={markdown} />
);
